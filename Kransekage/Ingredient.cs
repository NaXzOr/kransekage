public class Ingredient
{
    public Ingredient(string name, double amount)
    {
        Name = name;
        Amount = amount;
    }
    public string Name { get;}
    public double Amount { get;}
}