using System.Text;

public class Recipe
{
    public string Name { get; }

    public Recipe(string name, List<Ingredient> ingredients)
    {
        Name = name;
        Ingredients = ingredients;
    }

    public List<Ingredient> Ingredients { get; set; }
    
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.AppendLine($"Name: {Name}");
        sb.AppendLine("Ingredients list");
        foreach (var ingredient in Ingredients)
        {
            sb.AppendLine($"{ingredient.Name} {ingredient.Amount}g");
        }

        return sb.ToString();
    }
}